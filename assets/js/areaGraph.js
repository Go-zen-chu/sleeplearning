
drawGraph = function(tsvstr){
	try{
		var margin = {top: 20, right: 20, bottom: 30, left: 20},
			width = 300 - margin.left - margin.right,
			height = 200 - margin.top - margin.bottom;

		var parseDate = d3.time.format("%d-%b-%y").parse;

		var x = d3.time.scale().range([0, width]);
		var y = d3.scale.linear().range([height, 0]);
		var xAxis = d3.svg.axis().scale(x).orient("bottom");
		var yAxis = d3.svg.axis().scale(y).orient("left");

		var area = d3.svg.area()
			.x(function(d) { return x(d.date); })
			.y0(height)
			.y1(function(d) { return y(d.close); });

		var svg = d3.select("#graphDiv").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
		  .append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var data = d3.tsv.parse(tsvstr);

		data.forEach(function(d) {
			d.date = parseDate(d.date);
			d.close = +d.close;
		  });

		x.domain(d3.extent(data, function(d) { return d.date; }));
		y.domain([0, d3.max(data, function(d) { return d.close; })]);
		
		svg.append("path")
		  .datum(data)
		  .attr("class", "area")
		  .attr("d", area);
		
		svg.append("g")
		  .attr("class", "x axis")
		  .attr("transform", "translate(0," + height + ")")
		  .call(xAxis);
		
		svg.append("g")
		  .attr("class", "y axis")
		  .call(yAxis)
		.append("text")
		  .attr("transform", "rotate(-90)")
		  .attr("y", 6)
		  .attr("dy", ".71em")
		  .style("text-anchor", "end")
		  .text("Price ($)");
	}catch(e){
		console.error(e);
		printStackTrace(e);
	}
}
document.addEventListener('DOMContentLoaded',function(event){
	var tsvstr = "date	close\n1-May-12	582.13\n30-Apr-12	583.98\n27-Apr-12	603.00\n26-Apr-12	607.70\n25-Apr-12	610.00\n24-Apr-12	560.28\n23-Apr-12	571.70\n20-Apr-12	572.98\n19-Apr-12	587.44\n18-Apr-12	608.34\n17-Apr-12	609.70\n16-Apr-12	580.13\n13-Apr-12	605.23\n12-Apr-12	622.77\n11-Apr-12	626.20\n10-Apr-12	628.44\n9-Apr-12	636.23\n5-Apr-12	633.68\n4-Apr-12	624.31\n3-Apr-12	629.32\n2-Apr-12	618.63\n30-Mar-12	599.55\n29-Mar-12	609.86\n28-Mar-12	617.62\n27-Mar-12	614.48\n26-Mar-12	606.98\n23-Mar-12	596.05\n";

	drawGraph(tsvstr);
	d3.selectAll("line, path").style({"fill":"none", "stroke":"#000"});
	d3.select(".area").style({"fill":"steelblue"});
});
