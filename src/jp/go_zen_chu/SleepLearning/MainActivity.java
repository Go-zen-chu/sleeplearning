package jp.go_zen_chu.SleepLearning;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.webkit.ConsoleMessage;
import android.webkit.ConsoleMessage.MessageLevel;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		WebView wv = (WebView)findViewById(R.id.mainWebView);
		wv.loadUrl("file:///android_asset/html/start.html");
		wv.setWebChromeClient(new WebChromeClient() {
			public boolean onConsoleMessage(ConsoleMessage cm) {
				switch (cm.messageLevel()) {
				case ERROR: Log.e("MyApp", cm.message() + " -- From line " + cm.lineNumber() + " of " + cm.sourceId()); break;
				default: Log.d("MyApp", cm.message() + " -- From line " + cm.lineNumber() + " of " + cm.sourceId() ); break;
				}
				return true;
			}
		});
		WebSettings ws = wv.getSettings();
		ws.setJavaScriptEnabled(true);
		ws.setDatabasePath("/data/data/" + this.getPackageName() + "/dbs/");
		ws.setDomStorageEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
